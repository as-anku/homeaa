#!/bin/bash

for file in $(git status --porcelain);
do
	sed -r -i 's/\s+//g' $file
done
